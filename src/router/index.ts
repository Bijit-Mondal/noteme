import { createRouter, createWebHistory } from '@ionic/vue-router';
import { RouteRecordRaw } from 'vue-router';
import TabsPage from '../views/TabsPage.vue';
import {useAuth} from '../api/auth';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: '/notes/login'
  },
  {
    path: '/notes/',
    component: TabsPage,
    children: [
      {
        path: '',
        redirect: '/notes/login'
      },
      {
        path: 'login',
        component: () => import('@/views/Login.vue')
      },
      {
        path: 'create',
        component: () => import('@/views/Create.vue'),
        meta: { requiresAuth: true }
      },
      {
        path: 'view',
        component: () => import('@/views/Notes.vue'),
        meta: { requiresAuth: true }
      },
      {
        path: 'register',
        component: () => import('@/views/Register.vue')
      }
    ]
  }
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes
});

router.beforeEach(async (to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  const {isLoggedIn} = useAuth();
  const isAuthenticated = await isLoggedIn();

  if (requiresAuth && !isAuthenticated) {
    next('/notes/login');
  } else {
    next();
  }
});

export default router;

