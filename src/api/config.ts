import { Client, Account,Databases, Storage } from "appwrite";

const client = new Client();
client.setEndpoint("http://139.144.2.133/v1").setProject("64375b632159f3318263");

const account = new Account(client);
const database = new Databases(client);
const storage = new Storage(client);

export { client, account, database, storage };
