import { Permission, Role,ID } from 'appwrite';
import { database } from "./config";

const DatabaseID = "6437de4caab81b8403ee";
const CollectionID = "6437df34100406e2e9ed";

export interface Note {
    topic?: string;
    subTopic?: string;
    content?: string;
}

export const useData = () => {
    const addCollection = async (topic: string, subTopic: string, content: string): Promise<void> => {
    try {
      const { $id } = await database.createDocument(DatabaseID, CollectionID,ID.unique(), {
        topic,
        subTopic,
        content,
      });
    } catch (err) {
      throw err;
    }
  };
    const getCollection = async (): Promise<Note[]> => {
        try {
            const response = await database.listDocuments(DatabaseID, CollectionID);
            const documents = response.documents as Note[];
            return documents;
        } catch (err) {
            throw err;
        }
    };

    return {
        addCollection,
        getCollection,
    };
};

