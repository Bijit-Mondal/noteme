import {ID} from "appwrite";
import {account} from "./config";
export const useAuth = () => {
    const createAccount = async (email: string, password: string,name: string) => {
        try{
            await account.create(ID.unique(), email, password, name);
            await account.createEmailSession(email, password);
        } catch (e) {
            throw e;
        }
    };
    const login = async (email: string, password: string) => {
        try{
            return await account.createEmailSession(email, password);
        } catch (e) {
            throw e;
        }
    };
    const logout = async () => {
        try{
            return await account.deleteSession('current');
        } catch (e) {
            throw e;
        }
    };
    const isLoggedIn = async() =>{
        try{
            await account.get();
            return true;
        }catch(e){
            return false;
        }
    }
    return{
        createAccount,
        login,
        logout,
        isLoggedIn
    }
}

